package applications;

public class Main {

	public static void main(String args[]){
		
		View myView = new View();
		Controller controller = new Controller(myView);
		myView.setVisible(true);
	}
}
