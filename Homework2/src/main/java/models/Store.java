package models;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class Store extends Thread{
	
	private int nrOfQueues;
	private Queue[] queues ;
	private int minArrival;
	private int maxArrival;
	private int minService;
	private int maxService;
	private int simulationInterval;
	private Logger myLog;
	private float totalAverageTime;
	private int peakSecond = 0;
	private int emptyQueueTime;
	
	public Store(int number){
		
		this.nrOfQueues = number;
		this.queues= new Queue[nrOfQueues];
		int i;
		for(i=0; i<nrOfQueues; i++){
			queues[i] = new Queue("Queue" + Integer.toString(i));
		}
	}
	
    public void setLogger(Logger thisLogger){
		
		this.myLog = thisLogger; //set logger for the store 
		for(int i=0;i<nrOfQueues;i++){
			
			queues[i].setLogger(myLog); //set the same logger for the queues
		}
	}
	
	public void setIntervals(int minA, int maxA,int minS, int maxS, int simI){
		
		this.minArrival = minA;
		this.maxArrival = maxA;
		this.minService = minS;
		this.maxService = maxS;
		this.simulationInterval = simI;
		for(int j=0; j<nrOfQueues; j++)
			queues[j].start(); //start all threads when intervals are done selecting
	}
	
	public Queue[] getQueues() {
		
		return queues;
	}
	
	public void run(){
		
		Random random = new Random();
		int arrivalClient = 0;
		int serviceClient =0;
		int id = 1;
		int i = 1;
		int arrival;
		int nrOfClients = 0;
		myLog.startMessage(); //start message for the simulation
		myLog.setTimeReference(TimeUnit.SECONDS.convert(System.nanoTime(), TimeUnit.NANOSECONDS)); //set reference time as current time of the system
		while(true){
			
			arrival = random.nextInt(maxArrival - minArrival +1) + minArrival; //generate random arrival time
			serviceClient = random.nextInt(maxService - minService + 1) + minService; //generate random service
			
			arrivalClient = i + arrival; //compute client arrival as the sum of the current moment and the random generate number of second for next arrival
			updateWaitingTimes(arrival);
			i=i+arrival;
			int total = getTotalNumberOfClients(); //compute peak second as a maximum for total number of clients at a given moment
			if(total>nrOfClients) {
				
				peakSecond = i;
				nrOfClients = total;
			}
			try{
			Client C = new Client(id++, arrivalClient, serviceClient); //create new Client object 
			if(nrOfEmptyQueues()){
				emptyQueueTime += arrival; //total empty queue time is summed
			}
			System.out.println(emptyQueueTime);
			sleep(arrival*1000); //sleep for the necessary number of second until adding to queue
			moveToQueue(C); //add in queue the client 
			if(i>=simulationInterval) break; //break if simulation is done
			}
			catch(InterruptedException e){
				
				System.out.println("Interrup exception occured");
			}
		}
		myLog.stopMessage();  //stop message displayed
		float average=0;
		for(int k=0; k<nrOfQueues; k++){
			
			average = average + queues[k].getFinalAverage(); 
		}
		totalAverageTime = average/nrOfQueues; //total average waiting time is computed as the average waiting time for each queue

}
	private synchronized void moveToQueue(Client C) throws InterruptedException {
		
		int minServiceQ = 0;
		for(int j=0; j<nrOfQueues; j++){
			
			if(queues[j].getTotalServiceTime()<queues[minServiceQ].getTotalServiceTime()){
				minServiceQ = j;  //select the minimum queue in terms of total service time
			}
		}
		queues[minServiceQ].addClient(C); //add the client tot the found minimum queue
		myLog.addClientMessage(C, C.getService(), queues[minServiceQ]); //display an adding message
		notifyAll();
	}
	
	private synchronized void updateWaitingTimes(int arrival) {
		
		for(int i=0; i<nrOfQueues; i++){
			try{
			for(Client currentClient:queues[i].getListOfClients()){
				if(currentClient != queues[i].getListOfClients().get(0))
				currentClient.setTotalTime(arrival); //add the waiting time(addition is performed internally) for each client who is currently waiting and is not on the first position
				System.out.println(currentClient.getID() + " " + currentClient.getTotalTime());
			}
			}catch(ConcurrentModificationException e) {}
		}
	}
	
	public synchronized int getTotalNumberOfClients() {
		
		int k=0;
		for(int i=0; i<nrOfQueues; i++){
			
			k = k + queues[i].getSize(); 
		}
		return k;
	}
	
	private boolean nrOfEmptyQueues() {
		
		int nr=0;
		for(int i=0; i<nrOfQueues; i++){
			
			if(queues[i].getTotalServiceTime()==0)
				nr++; //increase only when we find an empty queue(with service time 0 in total)
		}
		if(nr==nrOfQueues){
			return true;
		}
		return false;
	}
	
	public float getTotalAverageTime() {
		
		return totalAverageTime;
	}
	
	public int getPeakSecond() {
		
		return peakSecond;
	}
	
	public int getEmptyQueueTime() {
		
		return emptyQueueTime;
	}
	
}